public class ShirtTester {

    public static void main(String [] args)
    {
        Shirt myShirt = new Shirt();
        Shirt yourShirt = new Shirt();
        myShirt.shirtID = 1;
        yourShirt.shirtID = 2;
        myShirt.price=5000;
        myShirt.des = "Pure Cotton";
        myShirt.colorCode ='B';
        myShirt.displayShirtInfo();
        yourShirt.displayShirtInfo();

    }
}
